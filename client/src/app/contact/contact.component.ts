import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from '../http.service';
import { Observable } from 'rxjs/Observable';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/operator/retry';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  allPosts$: Observable<Array<Msg>>;
  isRwd: boolean;

  @ViewChild('contactForm')
  contactForm: NgForm;

  message = new TemplateMessage;

  constructor(
    private httpService: HttpService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.getScreenInfo();
  }

  private getScreenInfo(): void {
    const w = window.innerWidth;
    const h = window.innerHeight;

    if (w < 768) {
      this.isRwd = true;
    }
  }

  private onSubmit(): void {
    const obj: Msg = this.prepareObjectToSave(this.message);

    this.httpService.addMsg(obj).subscribe((post: Msg) => {
      if (post != null) {
        this.toastr.success('Zgłoszenie zostało dodane.');
        this.message = new TemplateMessage();
        this.contactForm.resetForm();
      } else {
        this.toastr.error('Zgłoszenie nie zostało dodane.');
      }
    });
  }

  private prepareObjectToSave(message: any) {
    return {
      name: message.name,
      email: message.email,
      message: message.message
    };
  }

}

class TemplateMessage {
  constructor(
    public message?: string,
    public name?: string,
    public email?: string,
  ) {}
}

export interface Msg {
  name?: string;
  email?: string;
  message?: string;
}
