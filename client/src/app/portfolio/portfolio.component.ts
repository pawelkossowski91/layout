import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  allType: boolean;
  firstType: boolean;
  secondType: boolean;
  thirdType: boolean;

  constructor() { }
  ngOnInit() {
  }

  private onFiltr(event: any): void {
    const filtrType: string = event.target.id;

    if (filtrType === 'all') {
      this.allType = !this.allType;
    }

    if (filtrType === 'first') {
      this.firstType = !this.firstType;
    }

    if (filtrType === 'second') {
      this.secondType = !this.secondType;
    }

    if (filtrType === 'third') {
      this.thirdType = !this.thirdType;
    }
  }

}
