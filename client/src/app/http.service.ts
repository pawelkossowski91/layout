import { Injectable } from '@angular/core';
import { Msg } from '../app/contact/contact.component';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class HttpService {
  private postsObs = new BehaviorSubject<Array<Msg>>([]);
  posts$ = this.postsObs.asObservable();

  constructor(private http: HttpClient) {
    this.getMessages();
  }

  public getMessages() {
    return this.http.get<Array<Msg>>('http://localhost:3000/messages').subscribe(
      posts => {
        this.postsObs.next(posts);
        console.log('Aktualne wiadomości: ', posts);
      },
      err => {
        console.log(err);
      }
    );
  }


  public addMsg(post: Msg): Observable<Msg> {
    return this.http.post<Msg>('http://localhost:3000/messages', post);
  }
}
