import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  homeActive: boolean;
  offerActive: boolean;
  portfolioActive: boolean;
  contactActive: boolean;
  lastClicked: number;

  constructor() { }

  ngOnInit() {
    this.homeActive = true;
    this.offerActive = false;
    this.portfolioActive = false;
    this.contactActive = false;
    this.lastClicked = 0;
  }

  private onClick(event: any): void {
    const currentDiv: any = event.target.id;

    if ((currentDiv == 0) && currentDiv != this.lastClicked) {
      if (this.homeActive == false) {
        this.homeActive = !this.homeActive;
        this.offerActive = false;
        this.portfolioActive = false;
        this.contactActive = false;
      }
    }

    if ((currentDiv == 1) && currentDiv != this.lastClicked ) {
        this.homeActive = false;
        this.offerActive = !this.offerActive;
        this.portfolioActive = false;
        this.contactActive = false;
    }

    if ((currentDiv == 2) && currentDiv != this.lastClicked) {
      this.homeActive = false;
      this.offerActive = false;
      this.portfolioActive = !this.portfolioActive;
      this.contactActive = false;
    }

    if ((currentDiv == 3) && currentDiv != this.lastClicked) {
      this.homeActive = false;
      this.offerActive = false;
      this.portfolioActive = false;
      this.contactActive = !this.contactActive;
    }

    this.lastClicked = currentDiv;
  }
}
